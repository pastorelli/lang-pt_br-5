# Language Pack do Magento2 em Português do Brasil (pt_BR)
Tradução do Magento2 para português do Brasil. Atualziado a partir do repositório da Flowcommerce.

# Instalação

**Manual:**
- Efetue o download do repositório https://bitbucket.org/biz-commerce/lang-pt_br/
- Mova o conteúdo do repositório para a pasta app/i18n/BizCommerce/pt_BR
- No admin, vá em edição do usuário e escolha a linguagem



**Composer**

- `composer config repositories.bizcommerce-lang-pt_br git https://bitbucket.org/biz-commerce/lang-pt_br.git`
- `composer require biz-commerce/lang-pt_br:dev-master`
- `php bin/magento setup:static-content:deploy pt_BR ` 
- `php bin/magento setup:upgrade`
- `php bin/magento cache:flush`

